const scrollData;

/**
* Returns parsed JSON object as a manipulatable javascript OBJ.
*
* @param {string} file The file path to the json file.
* @param {function} callback A callback function to set the parsed json data to a variable.
* @return {object} A parsed json Object.
*/

function readTextFile(file,callback)
{
    let rawFile = new XMLHttpRequest();
    rawFile.overrideMimeType("application/json");
    rawFile.open("GET", file, true);
    rawFile.onreadystatechange = function() {
        if (rawFile.readyState === 4 && rawFile.status == "200") {
            callback(rawFile.responseText);
        }
    }
    rawFile.send(null);
}

readTextFile('scroll.json', function(text){
    scrollData = JSON.parse(text);
});

//Randomizing a number that is between 0 and the length of the array
let randomInt = Math.floor(Math.random()*(scrollData.Quotes.length));

//Setting the choosen quote the randomized index
let choosenQuote = scrollData.Quotes[randomInt]; 

const Discord = require('discord.js');
const client = new Discord.Client();

client.on('ready', () => {
    console.log('Connection Succesful.');
});

client.on('interactionCreate', async interaction =>{
    if(!interaction.isCommand()) return;
    if(interaction.commandName.toLowerCase() === "tas")
        await interaction.reply(`${choosenQuote.name}-${choosenQuote.Quote}`);
});

client.login();